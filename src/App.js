import React, {useState,useEffect} from 'react';
import axios from 'axios';
import './App.css';


const App = () => {
    const [resource, setresource] = useState('');
    const fetchAdvice = async() =>{

    const response = await axios.get('https://api.adviceslip.com/advice');
    setresource(response.data.slip.advice);
    
    }
    useEffect(() => {
        fetchAdvice();
    
      }, [])

    return (
        <div className="app">
            <div className="card">

<h2 className="heading">{resource}</h2>
<button onClick={fetchAdvice} className="button">Get Me An Advice</button>


            </div>
        </div>
    )
}
export default App;